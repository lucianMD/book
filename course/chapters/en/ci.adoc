[[continuous_integration]]
== Continuous Integration

Continuous integration allows to build, test and eventually deliver and deploy one or more systems.
This, in a *automatic*, *reliable* and *systematic* way.

This is what the typical workflow of a developer working with a continuous integration server looks like.
Let's assume that the developer has to implement a new feature called `F`.

. First, the developer updates his local copy with code from the central source repository
. Then, it implements `F`, coding at the same time:
.. the feature `F` itself;
.. the code testing the implementation of `F`.
. Then it performs a local build.
  This build not only performs the usual compilation phase, but also executes the tests.
. If the tests are OK, it can update its local copy.
  This is because other developers may well have changed the code in the central repository while he was implementing `F`.
.. If there are new things, the developer merges them into his own code.
   Then, he returns to step _3_.
.. Otherwise, he commits his implementation of `F`.
. Following this commit, the continuous integration will automatically build the system (and thus start testing).
.. If the build fails, the developer must fix the problem which causes his build to run locally,
   but it fails on the server. So: back to step _2_.
.. If the build passes, and only then, `F` implementation can be considered as completed!

In order to reach this point, it is necessary to take into account several points in terms of
configuration of the continuous integration server,
of the actual build mechanism,
of how the developers are going to commit,
and the visibility offered by continuous integration.

=== Configuration

* Continuous Integration requires the imperative use of a source manager.
  The source manager chosen depends on the preferences of the development team.
  However, it is important that only one reference branch is identified.
  It is this branch, frequently called _master_, that will be used for delivery.
* Similarly, it is necessary to have a continuous integration server.
  This continuous integration server must :
** have access to everything needed to build and test the project fully.
** provide as many tools as possible to the members of the development team.

=== Build

The build must imperatively be automatic.
In other words, this means that any changes to the system must always trigger a build.

The benefits are multiple:

* The build mechanism can be very complex:
  requiring many dependencies,
  cut into numerous conditional phases,
  targeting several different platforms,
  integrating a deployment pipeline... +
  Any automation of this process is that much less to be managed by the development team.
* An automatic build is the best way to avoid human errors.
* Test execution is an integral part of the build.
* A continuous integration server can be configured to build only what is needed.
  (i.e., only what has been changed, and what depends on it).
  This increases the speed with which the product can be delivered.

=== Commits

Good day-to-day use of an integration server translates into a coding discipline summarized by the axiom _"commit early, commit often"_: this means that any modification should be committed, in the most immediate and *atomic* way possible.

The benefits are as follows:

* If one or more tests fail after committing a few lines,
  the error is easier to analyze and fix than if the commit was ten lines or more.
* Forcing yourself to commit often usually leads to better code splitting, and therefore better design.
* Committing often in a useful way enriches the history maintained by the source manager,
  which facilitates analysis for future corrections or developments.
  This last point obviously presupposes the presence of informative commit comments.

=== Visibility

Continuous integration provides transparency.

Transparent for the members of the development team themselves, who always know where they are going,
because they benefit both from the customer's feedback and from their continuous integration.

It also means being visible outside the development team,
for example, by making artifacts available in appropriate locations to those who need them, when they need them.
